[General]
version = 2
blueprintrepositories = https://invent.kde.org/packaging/craft-blueprints-kde.git|master|

# Core
[virtual/base]

# For everything that uses MIME information
[libs/shared-mime-info]

# Everything wants Qt 6
[libs/qt6]

# Everything in Frameworks also needs Doxygen for QCH generation
[dev-utils/doxygen]

# ECM
[dev-utils/icoutils]

# KArchive
[libs/libarchive]
[libs/liblzma]

# KI18n
[libs/gettext]
[data/iso-codes]

# Solid
[dev-utils/flexbison]

# Prison
[libs/qrencode]
[libs/libdmtx]
[libs/zxing-cpp]

# KDocTools
[libs/libxslt]
[libs/libxml2]
[data/docbook-dtd]
[data/docbook-xsl]
[perl-modules/uri-url]
[perl-modules/xml-parser]

# KNotification
[dev-utils/snoretoast]

# KWallet
[libs/gcrypt]

# KActivities, Codevis, Calligra and others
[libs/boost]

# KCodecs
[dev-utils/gperf]

# Purpose
[kdesupport/qca]

# Breeze Icons
[python-modules/lxml]

# KFileMetaData
[libs/taglib]

# libkexiv2
[libs/exiv2]

# libkdcraw
[libs/libraw]

# KStars
[libs/eigen3]
[libs/wcslib]
[libs/libxisf]
[libs/libnova]
[libs/cfitsio]
# Does not support Qt6 yet
# [libs/stellarsolver]

# Minuet
[libs/glib]
[libs/fluidsynth]

# KCalc
[libs/mpfr]

# KCalCore
[libs/libical]

# KMailTransport, KLDAP and PIM Sieve Editor, NeoChat, Ruqola, etc.
[qt-libs/qtkeychain]

# Analitza
[libs/glew]

# Okular
[libs/ghostscript]
[qt-libs/poppler]
[libs/ebook-tools]
[libs/chm]
[libs/discount]
[libs/djvu]
[libs/libspectre]

# Kiten
[libs/mman]

# Lokalize
[libs/hunspell]

# Codevis
[qt-libs/quazip]
[libs/zlib]
[libs/catch2]

# KDevelop, Codevis
[libs/llvm]

# Labplot
[libs/gsl]

# Skrooge
[dev-utils/pkg-config]

# KCalc, KMyMoney & Alkimia
[libs/libgmp]
[libs/mpc]
[libs/sqlcipher]
[libs/libofx]

# Elisa
[binary/vlc]

# Digikam, Kdenlive
[libs/ffmpeg]

# Digikam
[libs/sqlite]
[libs/x265]
[libs/tiff]
[libs/expat]
[libs/lcms2]
[libs/opencv]
[libs/lensfun]
[libs/openal-soft]
[libs/libjpeg-turbo]

# NeoChat
[qt-libs/libquotient]
[qt-libs/qcoro]
[libs/cmark]

# RKWard
[binary/r-base]

# libqgit2
[libs/libgit2]

# Glaxnimate
[libs/potrace]

## Kaidan
[qt-libs/qxmpp]
[libs/libomemo-c]

## KRDC
[libs/libvncserver]
[libs/freerdp]

# kio-extras, KRDC
[libs/libssh]

# kio-extras
[libs/openexr]

# Ruqola
[libs/kdsingleapplication]
